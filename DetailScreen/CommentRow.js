import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Animated,
} from 'react-native';

import { convertToRelativeTime } from '../MainScreen/Utils';

import HTMLView from 'react-native-htmlview';

export default class CommentRow extends React.Component {
    animateCollapseComment = () => {
        Animated.timing(0, {
            toValue: 1,
            duration: 400,
            easing: Easing.linear,
            useNativeDriver: true
        }).start();
    };

    animateExpandComment = () => {
        Animated.timing(0, {
            toValue: 0,
            duration: 400,
            easing: Easing.linear,
            useNativeDriver: true
        }).start();
    };

    render() {
        const comment = this.props.comment
        let marginLeft = comment.level * 8
        return(
            <View style={[styles.container, { marginLeft: marginLeft }]}>
                <View style={{ flexDirection: 'row'}}>
                    <Text style={styles.title}>{comment.by} {convertToRelativeTime(comment.time)}</Text>
                    <TouchableOpacity onPress={this.props.handleShowHide}>
                        <Text style={[styles.title, { marginLeft: 4 }]}>{comment.canShow ? `[hide]` : `[show]` }</Text>
                    </TouchableOpacity>
                </View>
                {
                comment.canShow &&
                    <View style={{ flexDirection: 'column' }}>
                        <HTMLView
                            value={comment.text}
                            stylesheet={styles.content}
                        />
                        {comment.kids && 
                            <TouchableOpacity onPress={() => this.props.loadChildComments(comment)}>
                            <Text style={styles.reply}>{comment.childExpanded ? 'Hide replies' : 'Show replies'}</Text>
                        </TouchableOpacity>}
                    </View>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        margin: 8
    },
    title: {
        fontSize: 12,
        color: 'darkgrey',
        marginBottom: 16,
    },
    content: {
        fontSize: 15,
        color: 'black'
    },
    reply: {
        fontSize: 11,
        textDecorationLine: 'underline',
        marginTop: 8,
        marginBottom: 16,
    },    
})