import React from 'react';
import {
    SafeAreaView,
    FlatList,
    RefreshControl,
    StyleSheet,
    Text,
    ActivityIndicator,
    View
} from 'react-native';

import { fetchComments } from '../Service/CommentService';
import CommentRow from './CommentRow';
import Header from './DetailHeaderView';

import { get, chunk } from 'lodash';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class DetailScreenComponent extends React.Component {
    constructor(props) {
        super(props)
        this.page = 0;
        this.chunkingIdArray = [[]]
        this.item = this.props.navigation.getParam('item')
    }

    state = {
        isLoading: true,
        data: []
    }

    componentDidMount() {
        this.chunkingIdArray = chunk(this.item.kids, 3)
        this.onRefresh()
    }

    renderRefreshControl() {
        return (
            <RefreshControl
                refreshing={this.state.isLoading}
                onRefresh={this.getStoriesIds}
            />
        )
    }

    onRefresh() {
        this.resetAllData()
        this.getComments()
    }

    resetAllData() {
        this.page = 0
    }

    renderFooter = () => {
        if (!this.state.data.length) {
            return null
        }
        //it will show indicator at the bottom of the list when data is loading otherwise it returns null
        if (!this.state.isLoading) return null;
        return (
            <ActivityIndicator
                style={{ color: 'red' }}
            />
        );
    };

    renderHeader = (comment) => {
        return (
            <Header item={comment}/>
        );
    };

    handleLoadMore = () => {
        if (this.page >= this.chunkingIdArray.length) {
            this.setState({
                isLoading: false
            })
            return null
        }

        if (!this.state.isLoading) {
            this.page = this.page + 1
            this.getComments()
        }
    }

    getComments = async () => {
        this.setState({
            isLoading: true
        })
        try {
            const ids = get(this.chunkingIdArray, `${this.page}`);
            if (!ids) {
                return null
            }
            const comments = await Promise.all(ids.map(e => fetchComments(e)));
            let mapped = comments.map(item => ({
                ...item, //copy all properties
                canShow: true, //Hide all childs and text. Only show username.
                level: 1, //level of comment
                childExpanded: false, //Child commment is expanded or not
            }))
            let updatedComments = this.state.data.concat(mapped);
            this.setState({
                isLoading: false,
                data: updatedComments
            })
        } catch (e) {
            console.log('==>', e)
            this.setState({ isLoading: false })
        }
    }

    renderRow(comment, index) {
        return (
            <CommentRow
                comment={comment}
                handleShowHide = { () => {
                    let temp = this.state.data
                    temp[index].canShow = !temp[index].canShow

                    this.setState({
                        data: temp
                    })

                    this.loadChildComments(comment)
                }}
                loadChildComments = { this.loadChildComments }
            />
        )
    }

    loadChildComments = async (comment) => {
        let data = this.state.data
        let index = data.findIndex(x => x.id == comment.id)

        if (data[index].childExpanded) {
            data[index].childExpanded = false
            let allChildren = this.getAllChildren(comment)
            let result = data.filter( item => !allChildren.includes(item))
            this.setState({
                data: result
            })
            return null
        }

        data[index].childExpanded = true

        const comments = await Promise.all(comment.kids.map(e => fetchComments(e)));
        let mapped = comments.map(item => ({
            ...item, //copy all properties
            canShow: true, //Hide all childs and text. Only show username. 
            level: comment.level + 1, //level of comment
            childExpanded: false, //Child commment is expanded or not
        }))
        let merged = this.mergedAtIndex(data, index+1, mapped)

        this.setState({
            data: merged
        })
    }

    getAllChildren(comment) {
        let temp = this.state.data
        let beginToCheck = false
        let returnValue = []

        for (i=0; i<temp.length;i++) {
            let item = temp[i]

            if (item.id == comment.id) {
                beginToCheck = true
                continue
            }
            if (beginToCheck && item.level <= comment.level) {
                break
            }
            if (beginToCheck) {
                returnValue.push(item)
            }
        }
        return returnValue
    }

    mergedAtIndex(list, index, values) {
        let firstHalf = list.slice(0, index)
        let secondHalf = list.slice(index, list.length)
        let merged = firstHalf.concat(values)
        return merged.concat(secondHalf)
    }

    render() { 
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                    <Text style={{color: 'red', margin: 8, fontSize: 18}}>Back</Text>
                </TouchableOpacity>
                <FlatList
                    style={{ backgroundColor: 'floralwhite', flex: 1 }}
                    data={this.state.data}
                    renderItem={({ item, index }) => this.renderRow(item, index)}
                    keyExtractor={(_, index) => `list-item-${index}`}
                    refreshControl={this.renderRefreshControl()}
                    ItemSeparatorComponent={this.renderSeparator}
                    onEndReachedThreshold={0.3}
                    onEndReached={this.handleLoadMore}
                    ListFooterComponent={this.renderFooter}
                    ListHeaderComponent={this.renderHeader(this.item)}
                    initialNumToRender={8}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'floralwhite',
    },
    rowContainer: {
        flexDirection: 'row',
        margin: 8
    },
    columnContainer: {
        flexDirection: 'column',
        margin: 8
    },
    titleLabel: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    rowText: {
        margin: 8,
        color: 'darkgray',
        fontSize: 13,
    }
})