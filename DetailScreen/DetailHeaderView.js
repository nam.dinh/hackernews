import React from 'react';
import {
    SafeAreaView,
    FlatList,
    RefreshControl,
    StyleSheet,
    Text,
    ActivityIndicator,
    View
} from 'react-native';

import { convertToRelativeTime, convertScore } from '../MainScreen/Utils';

export default class DetailHeaderView extends React.Component {
    render() {
        let item = this.props.item
        return(
            <View style={styles.columnContainer}>
                <View style={styles.rowContainer}>
                    <Text style={styles.titleLabel}>{item.title}</Text>
                </View>
                <Text style={styles.rowText}>{convertScore(item.score)} by {item.by} | {convertToRelativeTime(item.time)} | {item.descendants} comments</Text>
                <Text style={{ margin: 8 }}>{item.text}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    rowContainer: {
        flexDirection: 'row',
        margin: 8,
        flex: 1,
    },
    columnContainer: {
        flexDirection: 'column',
    },
    titleLabel: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    rowText: {
        margin: 8,
        color: 'darkgray',
        fontSize: 13,
    },
})