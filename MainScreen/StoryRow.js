import React from 'react';
import { 
    View, 
    Text, 
    StyleSheet,
    Image, 
    TouchableOpacity,
    Linking,
} from 'react-native';

// Third-party
import PropTypes from 'prop-types';
import { convertToRelativeTime, convertScore } from './Utils';

class StoryRow extends React.Component {

    static propTypes = {
        showDetail: PropTypes.func.isRequired,
    }
    handleOpenURL(url) {
        if (!url) {
            this.props.showDetail();
            return null
        }
        Linking.canOpenURL(url)
        .then(supported => {
            if (supported) {
                Linking.openURL(url)
            } else {
                console.log('Cannot open url', url)
                this.props.showDetail();
            }
        })
    }

    getBaseURL(url) {
        if (url) {
            var regex = /(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]/
            const domain = regex.exec(url)
            if (domain.length) {
                return `(${domain})`
            }
            return ''
        }
    }

    render() {
        let story = this.props.story        
        return(
            <View style={styles.container}>
                <View style={styles.titleView}>
                    <Text style={styles.countLabel}>{this.props.counter}.</Text>
                    <TouchableOpacity onPress={() => this.handleOpenURL(story.url)}>
                        <Text style={styles.titleLabel}>{story.title}</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={() => this.handleOpenURL(story.url)}>
                    <Text style={styles.linkLabel}>{this.getBaseURL(story.url)}</Text>
                </TouchableOpacity>
                <View style={[styles.titleView, { marginTop: 8, marginStart: 16, flexWrap:'wrap'}]}>
                    <Text style={styles.detailLabel}>{convertScore(story.score)}</Text>
                    <Text style={[styles.detailLabel, { marginStart: 4, marginEnd: 4 }]}>by</Text>
                    <TouchableOpacity>
                        <Text style={[styles.detailLabel, {textDecorationLine: 'underline'}]}>{story.by}</Text>
                    </TouchableOpacity>
                </View>
                <Text style={[styles.detailLabel, { marginStart: 16, marginEnd: 4 }]}>{convertToRelativeTime(story.time)}</Text>
                <TouchableOpacity onPress={() => this.props.showDetail()}>
                    <Text style={[styles.detailLabel, { marginStart: 16, textDecorationLine: 'underline' }]}>{story.descendants} comments</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 8,
        flexDirection: 'column',
        alignItems: 'flex-start'      
    },
    titleView: {
        flex: 1,
        flexDirection: 'row',
        marginRight: 8,
    },
    countLabel: {
        color: 'darkgrey',
        fontSize: 18,
    },
    linkLabel: {
        color: 'darkgrey',
        fontSize: 12,
        marginStart: 16,
    },
    detailLabel: {
        color: 'darkgrey',
        fontSize: 14,        
    },
    titleLabel: {
        color: 'black',
        fontSize: 18,
        marginStart: 4,
    },
    url: {
        
    }
});

export default StoryRow;