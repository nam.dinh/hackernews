import React from 'react';
import {
    StyleSheet,
} from 'react-native';

import { Dimensions, Platform, PixelRatio } from 'react-native';

import {
    fetchTopStoryIds,
    fetchNewStoryIds,
    fetchAskStoryIds,
    fetchShowStoryIds,
    fetchJobStoryIds,
    fetchStoryById
} from "../Service/StoryServices";

import TimeAgo from 'javascript-time-ago';
import en from 'javascript-time-ago/locale/en'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
    }
});

export const listType = {
    topStories: {
        value: 0,
        text: 'Top',
        api: fetchTopStoryIds(),
    },
    new: {
        value: 1,
        text: 'New',
        api: fetchNewStoryIds(),
    },
    ask: {
        value: 2,
        text: 'Ask',
        api: fetchAskStoryIds(),
    },
    show: {
        value: 3,
        text: 'Show',
        api: fetchShowStoryIds(),
    },
    job: {
        value: 4,
        text: 'Job',
        api: fetchJobStoryIds(),
    },
    none: {
        value: 5,
        text: '',
        api: null
    }
}

export function getListType(index) {
    var type = listType.none
    switch (index) {
        case listType.topStories.value:
            type = listType.topStories
            break
        case listType.new.value:
            type = listType.new
            break
        case listType.ask.value:
            type = listType.ask
            break
        case listType.show.value:
            type = listType.show
            break
        case listType.job.value:
            type = listType.job
            break
        default:
            break
    }
    return type
}

export const optionArray = [
    listType.topStories.text,
    listType.new.text,
    listType.ask.text,
    listType.show.text,
    listType.job.text,
    'Cancel'
]

export const baseWith = 230;
export const baseHeight = 568;
export const screen = Dimensions.get('screen');
export const primaryColor = '#FFFFFF';
export const secondColor = '#606060';
export const pixelSize = 1 / PixelRatio.getPixelSizeForLayoutSize(1);
export const borderColor = '#605F60';
export const inactiveTintColor = '#605F60';
export const bgLoadingColor = '#F6F7F9';
export const activeTintColor = '#FF0000';
export const textColor = '#333333';
export const statusBarHeight = Platform.select({ ios: 20, android: 24 });
export const navBarHeight = 55;
export const tabBarHeight = 56;

export const textStyle = {
    fontFamily: 'SF UI Display',
    fontSize: Scale(12),
    fontWeight: Platform.select({ ios: '300', android: '100' }),
    color: textColor
}

export function Scale(size = 12) {
    const scaleWith = screen.width / baseWith;
    const scaleHeight = screen.height / baseHeight;
    const scale = Math.min(scaleWith, scaleHeight);
    return Math.ceil(scale * (size + Platform.select({ ios: 1, android: 0 })));
}

export function convertToRelativeTime(time) {
    TimeAgo.addLocale(en)
    const timeAgo = new TimeAgo('vi-VN')

    return timeAgo.format(new Date(time * 1000))
}

export function convertScore(score) {
    if (score == 1) {
        return score + ' point'
    }
    return score + ' points'
}