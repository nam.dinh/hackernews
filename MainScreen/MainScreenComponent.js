import React from 'react';
import { 
    SafeAreaView,
    FlatList, 
    RefreshControl,
    Text, 
    ActivityIndicator, 
    View 
} from 'react-native';

// Third-party
import { get, chunk } from 'lodash';
import ActionSheet from 'react-native-actionsheet';

//Local
import StoryRow from "./StoryRow";
import { 
    styles, 
    listType, 
    optionArray,
    getListType,
} from "./Utils";
import NavHeader from '../NavHeader';
import {fetchStoryById} from "../Service/StoryServices";

class MainScreenComponent extends React.Component {
    constructor(props) {
        super(props)

        this.page = 0;
        this.chunkingIdArray = [[]]
    }

    state = {
        isLoading: true,
        data: [],
        currentListType: listType.topStories,
    }

    componentDidMount() {
        this.onRefresh()
    }

    renderRefreshControl() {
        return (
            <RefreshControl
                refreshing={this.state.isLoading}
                onRefresh={this.getStoriesIds}
            />
        )
    }

    onRefresh() {
        if (!this.state.currentListType.api) {
            return null
        }
        this.resetAllData()
        this.getStoriesIds()
    }

    getStoriesIds = async() => {
        try {
            const ids = await this.state.currentListType.api
            if (ids.length) {
                this.chunkingIdArray = chunk(ids, 20)
                this.getStories()
            }
        } catch (e) {
            this.setState({ isLoading: false })
        }
    }

    getStories = async() => {
        try {
            const ids = get(this.chunkingIdArray, `${this.page}`);
            const stories = await Promise.all(ids.map(e => fetchStoryById(e)));             
            let updatedStories = this.state.data.concat(stories);
            this.setState({
                isLoading: false,
                data: updatedStories
            })
        } catch (e) {
            console.log('==>',e)
            this.setState({ isLoading: false })
        }
    }

    renderRow(story, index) {
        return(
            <StoryRow
                story = { story }
                counter = { index + 1 }
                showDetail = { () => {
                    this.props.navigation.push('Details', { item: story })
                }}                
            />
        )
    }

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: '100%',
                    backgroundColor: '#CED0CE',
                    marginLeft: 8
                }}
            />
        );
    };

    renderFooter = () => {
        if (!this.state.data.length) {
            return null
        }
        //it will show indicator at the bottom of the list when data is loading otherwise it returns null
        if (!this.state.isLoading) return null;
        return (
            <ActivityIndicator
                style={{ color: 'red' }}
            />
        );
    };

    handleLoadMore = () => {
        if (this.page >= this.chunkingIdArray.length) {
            this.setState({
                isLoading: false
            })
            return null
        }

        if (!this.state.isLoading) {
            this.page = this.page + 1
            this.getStoriesIds()
        }
    }

    showActionSheet = () => {
        this.ActionSheet.show()
    }

    setCurrentListType(index) {
        let type = getListType(index)
        if (type.api && this.state.currentListType.text != type.text) {
            this.setState({
                isLoading: true,
                data: [],
                currentListType: type
            }, () => {
                this.onRefresh()
            })
        }
    }

    resetAllData() {
        this.page = 0
        this.chunkingIdArray = [[]];
    }

    render() {
        const { navigate } = this.props.navigation
        return(
            <View style={{ flex: 1, backgroundColor: 'floralwhite' }}>
                <NavHeader 
                    title='Stories'
                    buttonTitle={this.state.currentListType.text}
                    onSelect={this.showActionSheet}
                />
                <FlatList 
                    style={{ backgroundColor: 'floralwhite', flex: 1 }}
                    key={this.state.currentListType.text}
                    data={this.state.data}
                    renderItem={({ item, index }) => this.renderRow(item, index)}
                    keyExtractor={(_, index) => `list-item-${index}`}
                    refreshControl={this.renderRefreshControl()}
                    ItemSeparatorComponent={this.renderSeparator}
                    onEndReachedThreshold={0.3}
                    onEndReached={this.handleLoadMore}
                    ListFooterComponent={this.renderFooter}
                    initialNumToRender={8}
                />

                <ActionSheet
                    ref={o => (this.ActionSheet = o)}
                    title='Stories sort by'
                    options={optionArray}
                    cancelButtonIndex={optionArray.length - 1}
                    onPress={index => {
                        this.setCurrentListType(index)
                    }}
                    
                />
            </View>
        )
    }
}

export default MainScreenComponent;