export const fetchComments = async (id) => {
    let ids = [];
    try {
        const response = await fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`);
        ids = await response.json();
    } catch (e) {
        console.log('catch', e)
    }

    return ids
}