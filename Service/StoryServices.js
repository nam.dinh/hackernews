export const fetchTopStoryIds = async () => {
    let ids;
    try {
        const response = await fetch(`https://hacker-news.firebaseio.com/v0/topstories.json`);
        ids = await response.json();
    } catch (e) {
        console.log(e)
    }

    return ids
}

export const fetchNewStoryIds = async () => {
    let ids;
    try {
        const response = await fetch(`https://hacker-news.firebaseio.com/v0/newstories.json`);
        ids = await response.json();
    } catch (e) {
        console.log(e)
    }

    return ids
}

export const fetchAskStoryIds = async () => {
    let ids;
    try {
        const response = await fetch(`https://hacker-news.firebaseio.com/v0/askstories.json`);
        ids = await response.json();
    } catch (e) {
        console.log(e)
    }

    return ids
}

export const fetchShowStoryIds = async () => {
    let ids;
    try {
        const response = await fetch(`https://hacker-news.firebaseio.com/v0/showstories.json`);
        ids = await response.json();
    } catch (e) {
        console.log(e)
    }

    return ids
}

export const fetchJobStoryIds = async () => {
    let ids;
    try {
        const response = await fetch(`https://hacker-news.firebaseio.com/v0/jobstories.json`);
        ids = await response.json();
    } catch (e) {
        console.log(e)
    }

    return ids
}

export const fetchStoryById = async(id) => {
    let story;
    try {
        const response = await fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`);
        story = await response.json();
    } catch (e) {
        console.log(e)
    }
    return story
}