/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { createAppContainer } from 'react-navigation';
import { createStackNavigator, } from 'react-navigation-stack';

import MainScreenComponent from './MainScreen/MainScreenComponent';
import DetailScreenComponent from './DetailScreen/DetailScreenComponent';

import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { SafeAreaView } from 'react-navigation';

const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: MainScreenComponent
    },
    Details: {
      screen: DetailScreenComponent
    }
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
  }
);

function InjectMain(Com) {
  class InjMan extends Component {
    render() {
      return (
        <SafeAreaView forceInset={{bottom: 'never'}} style={{ flexGrow: 1 }}>
          <Com {...this.props} />
        </SafeAreaView>
      )
    }
  }
  return InjMan;
}

export default InjectMain(createAppContainer(AppNavigator));