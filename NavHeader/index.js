import React from 'react';
import {
    View,
    Button,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

export default class NavHeader extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity>
                    <Text style={styles.leftButton}>
                    </Text>
                </TouchableOpacity>
                <Text style={styles.title}>
                    {this.props.title}
                </Text>
                <TouchableOpacity onPress={this.props.onSelect}>
                    <Text style={styles.rightButton}>
                        {this.props.buttonTitle}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 55,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        borderBottomWidth: 0.5,
        borderColor: 'grey',
        borderRadius: 2,
    },
    leftButton: {
        color: 'red',
        width: 44,
        marginLeft: 16,
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold'
    },  
    rightButton: {
        color: 'red',
        width: 44,
        marginRight: 16,
        textAlign: 'right'
    }
})